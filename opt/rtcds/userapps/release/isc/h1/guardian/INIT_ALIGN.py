"""One-click to initial alignment.

Needs to grab management of the proper nodes,
and make sure it wont be fighting with other nodes.

Should mainly just tell ALIGN_IFO when to go from 
different IA states.

"""
from guardian import GuardState, NodeManager
from ISC_library import unstall_nodes
import time

####################
# Manage

node_list = ['ALIGN_IFO', 'ALS_XARM', 'ALS_YARM']

nodes = NodeManager(node_list)

nominal = 'IDLE'
#############################################
# States
#############################################

def gen_idle_state(index, goto_bool=False):
    class IDLE(GuardState):
        """General idle state used as a stopping place or stepping stone.

        """
        goto = goto_bool
        def main(self):
            return True
        def run(self):
            return True
    IDLE.index = index
    return IDLE


class INIT(GuardState):
    """

    """
    def main(self):
        nodes.set_managed()
        nodes['ALIGN_IFO'] = 'INIT'

    def run(self):
        return True


class DOWN(GuardState):
    """Use as a reset in case we need to manual and need to try again.

    """
    goto = True
    def main(self):
        for xy in ['X', 'Y']:
            nodes['ALS_{}ARM'.format(xy)] = 'UNLOCKED'
        nodes['ALIGN_IFO'] = 'DOWN'

    @nodes.checker()
    def run(self):
        if nodes.arrived:
            return True


IDLE = gen_idle_state(5, goto_bool=True)

####################
# Green

# FIXME: Green is not ready for the wfs to be offloaded automatically yet!
GREEN_ARMS_OFFLOADED = gen_idle_state(10)


class PREP_FOR_GREEN(GuardState):
    """Move the optics into place and set the ALS arms to unlocked.

    """
    request = False
    index = 7
    def main(self):
        nodes['ALIGN_IFO'] = 'SET_SUS_FOR_ALS_FPMI'
        for xy in ['X', 'Y']:
            nodes['ALS_{}ARM'.format(xy)] = 'UNLOCKED'

    @nodes.checker()        
    def run(self):
        if nodes.arrived:
            return True
        # May need to add an unstall here, lets see if its necessary first


class LOCKING_GREEN_ARMS(GuardState):
    """Bring the green arms (ALS_?ARM) to GREEN_WFS_OFFLOADED.
    We may need to think about a way to troubleshoot, it wont 
    be that easy.
    """
    request = False
    index = 8
    def main(self):
        self.final_arm_req = 'INITIAL_ALIGNMENT'
        self.locked_states_list = ['INITIAL_ALIGNMENT',
                                   'ENABLE_WFS',
                                   'OFFLOAD_GREEN_WFS',
                                   'INITIAL_ALIGNMENT_OFFLOADED']
        # Set timer before increased flashes starts
        incflashes_time = 60

        for xy in ['X', 'Y']:
            nodes[f'ALS_{xy}ARM'] = 'INITIAL_ALIGNMENT'
            self.timer[f'{xy.lower()}_not_locked'] = incflashes_time
            self.timer[f'{xy.lower()}_locked_but_no_wfs'] = 0

        # 0 = initialize, 1 = timer set, 2 = IF requested (this is gross)
        self.no_wfs_if_attempt = {arm: 0 for arm in ['X', 'Y']}
        self.iflashes = {arm: 0 for arm in ['X', 'Y']}
        self.arms_complete = []

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        # Make it so we can offload one arm
        for arm in ['X', 'Y']:
            if nodes[f'ALS_{arm}ARM'].state == 'INITIAL_ALIGNMENT' \
                    and nodes[f'ALS_{arm}ARM'].done:
                if arm not in self.arms_complete:
                    self.arms_complete.append(arm)
                    nodes[f'ALS_{arm}ARM'] = 'INITIAL_ALIGNMENT_OFFLOADED'
        if 'X' in self.arms_complete and 'Y' in self.arms_complete:
            return True

        arms = ['X', 'Y']
        for arm in arms:
            if self.timer['{}_not_locked'.format(arm.lower())]:
                # Make sure that it isnt already locked, or hasnt tried IncFlashes 2x
                if nodes['ALS_{}ARM'.format(arm)].state not in self.locked_states_list \
                        and nodes['ALS_{}ARM'.format(arm)].request != 'SCAN_ALIGNMENT' \
                        and self.iflashes[arm] < 2:
                    # This should know to jump on its own
                    nodes['ALS_{}ARM'.format(arm)] = 'SCAN_ALIGNMENT'
                    self.iflashes[arm] += 1
                    log('Trying Increase Flashes {}arm ({}x)'.format(arm, self.iflashes[arm]))
                    # Need this here so it has time to see the state change
                    time.sleep(0.2)
                elif nodes['ALS_{}ARM'.format(arm)].state == 'SCAN_ALIGNMENT' \
                        and nodes['ALS_{}ARM'.format(arm)].done:
                    nodes['ALS_{}ARM'.format(arm)] = self.final_arm_req
                    # Give it some time to try to lock with this alignment
                    self.timer['{}_not_locked'.format(arm.lower())] = 90
                # FIXME: The comment below is a copy from ISC_LOCK. It isn't entirely applicable
                # here because this will almost always stay in ENABLE_WFS even while its flashing.
                ## We are locked but not with enough power for WFS to engage, do IF.
                # This isn't a good test. It could be going between locking and locked,
                # while still satisfying these conditions. BUT it should need to go to
                # IF anyway at that point. If we find that that isnt true, then we will
                # need to add in a check to see if the power is stable between the condition.
                elif nodes['ALS_{}ARM'.format(arm)].state == 'ENABLE_WFS' \
                        and 0.2 < ezca['ALS-{}_TR_A_LF_OUT16'.format(arm)] < 0.6 \
                        and self.timer['{}_locked_but_no_wfs'.format(arm.lower())] \
                        and self.no_wfs_if_attempt[arm] == 1:
                    nodes['ALS_{}ARM'.format(arm)] = 'SCAN_ALIGNMENT'
                    log('The power seems to be too low for the WFS to catch, trying IF')
                    self.iflashes[arm] += 1
                    self.no_wfs_if_attempt[arm] = 2
                # Set up timer for previous conditional
                elif nodes['ALS_{}ARM'.format(arm)].state == 'ENABLE_WFS' \
                        and 0.2 < ezca['ALS-{}_TR_A_LF_OUT16'.format(arm)] < 0.6 \
                        and self.timer['{}_locked_but_no_wfs'.format(arm.lower())] \
                        and self.no_wfs_if_attempt[arm] == 0:
                    self.timer['{}_locked_but_no_wfs'.format(arm.lower())] = 60
                    self.no_wfs_if_attempt[arm] = 1
                # Hands up, give up, your problem
                elif nodes['ALS_{}ARM'.format(arm)].request == self.final_arm_req \
                        and nodes['ALS_{}ARM'.format(arm)].state not in self.locked_states_list \
                        and self.iflashes[arm] >= 2:
                    notify('Find {} arm by hand?'.format(arm))


class OFFLOADING_GREEN_WFS(GuardState):
    """Offload em.

    """
    request = False
    index = 9
    def main(self):
        for xy in ['X', 'Y']:
            nodes['ALS_{}ARM'.format(xy)] = 'INITIAL_ALIGNMENT_OFFLOADED'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        # FIXME: When only one green node has offloaded but then loses lock, it will go through
        # the whole process again.
        for xy in ['X', 'Y']:
            if nodes.arrived:
                # Might as well put the green back to unlocked
                nodes['ALS_{}ARM'.format(xy)] = 'UNLOCKED'
        # Make sure both are done
        if nodes['ALS_YARM'].STATE == 'UNLOCKED' and\
           nodes['ALS_XARM'].STATE == 'UNLOCKED':
            return True


####################
# Input Align

INPUT_ALIGN_OFFLOADED = gen_idle_state(20)


class ALIGNING_INPUT(GuardState):
    """Input align

    """
    request = False
    index = 18    
    def main(self):
        nodes['ALIGN_IFO'] = 'INPUT_ALIGN'
        
        for xy in ['X', 'Y']:
            nodes['ALS_{}ARM'.format(xy)] = 'UNLOCKED'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if nodes.arrived and nodes['ALIGN_IFO'].done:
            return True


class OFFLOADING_INPUT_ALIGN(GuardState):
    """Offloading

    """
    request = False
    index = 19
    def main(self):
        nodes['ALIGN_IFO'] = 'INPUT_ALIGN_OFFLOADED'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if nodes.arrived and nodes['ALIGN_IFO'].done:
            return True


####################
# PRC Align

PRC_ALIGN_OFFLOADED = gen_idle_state(30)


class PRC_ALIGNING(GuardState):
    """PRC

    """
    request = False
    index = 28    
    def main(self):
        nodes['ALIGN_IFO'] = 'PRC_ALIGN'

        for xy in ['X', 'Y']:
            nodes['ALS_{}ARM'.format(xy)] = 'UNLOCKED'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if nodes.arrived and nodes['ALIGN_IFO'].done:
            return True


class OFFLOADING_PRC(GuardState):
    """Offloading

    """
    request = False
    index = 29
    def main(self):
        nodes['ALIGN_IFO'] = 'PRC_ALIGN_OFFLOADED'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if nodes.arrived and nodes['ALIGN_IFO'].done:
            return True


####################
# MICH Dark Align

MICH_BRIGHT_OFFLOADED = gen_idle_state(40)


class MICH_BRIGHT_ALIGNING(GuardState):
    """PRC

    """
    request = False
    index = 38    
    def main(self):
        nodes['ALIGN_IFO'] = 'MICH_BRIGHT_ALIGN'

        for xy in ['X', 'Y']:
            nodes['ALS_{}ARM'.format(xy)] = 'UNLOCKED'
        self.failed = False
        self.timer['chill out'] = 3 # in seconds
    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        # consider removing these timers and checks for DOWN after installing BS 
        # oplev check in PREP_FOR_MICH in ALIGN_IFO
        # JSK, TJS 2019-12-03
        if nodes['ALIGN_IFO'].STATE == 'DOWN' and self.failed == False and self.timer['chill out']:
            #ALIGN_IFO recognizes when MICH locking fails, and goes to DOWN, but then it needs to wait a minute
            self.timer['chill out'] = 45 # in seconds
            self.failed = True
            nodes['ALIGN_IFO'] = 'DOWN'
        elif self.failed == True and self.timer['chill out']:
            nodes['ALIGN_IFO'] = 'MICH_BRIGHT_ALIGN'
            self.failed = False
            self.timer['chill out'] = 3  # in seconds
        #elif nodes.arrived and nodes['ALIGN_IFO'].done:
        elif nodes.arrived and nodes['ALIGN_IFO'].STATE == 'MICH_BRIGHT_ALIGN':
            return True


class OFFLOADING_MICH_BRIGHT(GuardState):
    """Offloading

    """
    request = False
    index = 39
    def main(self):
        nodes['ALIGN_IFO'] = 'MICH_BRIGHT_OFFLOADED'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if nodes.arrived and nodes['ALIGN_IFO'].done:
            return True


####################
# SRC Align

SRC_ALIGN_OFFLOADED = gen_idle_state(50)


class SRC_ALIGNING(GuardState):
    """SRC

    """
    request = False
    index = 48    
    def main(self):
#        nodes['ALIGN_IFO'] = 'SRC_ALIGN'
        nodes['ALIGN_IFO'] = 'OFFLOAD_SR2_ALIGNMENT'

        for xy in ['X', 'Y']:
            nodes['ALS_{}ARM'.format(xy)] = 'UNLOCKED'

        self.SR2_finished = False
        self.AS_centering = False
        

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):

        if nodes.arrived and nodes['ALIGN_IFO'].done and not self.SR2_finished:
            self.SR2_finished = True
            nodes['ALIGN_IFO'] = 'AS_CENTERING_OFFLOADED'
        
        elif nodes.arrived and nodes['ALIGN_IFO'].done and self.SR2_finished and not self.AS_centering:
            self.AS_centering = True
            nodes['ALIGN_IFO'] = 'SRC_ALIGN'

        elif nodes.arrived and nodes['ALIGN_IFO'].done and self.SR2_finished and self.AS_centering:
            return True


class OFFLOADING_SRC(GuardState):
    """Offloading

    """
    request = False
    index = 49
    def main(self):
        nodes['ALIGN_IFO'] = 'SRC_ALIGN_OFFLOADED'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if nodes.arrived and nodes['ALIGN_IFO'].done:
            return True


####################
# End

INIT_ALIGN_COMPLETE = gen_idle_state(100)


####################
# If wanting to update snap file slider values for optics

class UPDATE_SAFE_SNAP(GuardState):
    """Updates the OPTICALIGN slider values in the safe.snap files"""
    request = True
    index = 105
    def main(self):
        # Call on ALIGN_IFO to update OPTIC_ALIGN values in the sdf
        nodes['ALIGN_IFO'] = 'UPDATE_SAFE_SNAP'
        
    @nodes.checker()
    @unstall_nodes(nodes)
    
    def run(self):
        if nodes.arrived and nodes['ALIGN_IFO'].done:
            return True
        
    
#############################################
# Edges

edges = [('DOWN', 'PREP_FOR_GREEN'),
         ('PREP_FOR_GREEN', 'LOCKING_GREEN_ARMS'),
         ('LOCKING_GREEN_ARMS', 'OFFLOADING_GREEN_WFS'),
         ('OFFLOADING_GREEN_WFS', 'GREEN_ARMS_OFFLOADED'),
         ('GREEN_ARMS_OFFLOADED', 'ALIGNING_INPUT'),
         ('ALIGNING_INPUT', 'OFFLOADING_INPUT_ALIGN'),
         ('OFFLOADING_INPUT_ALIGN', 'INPUT_ALIGN_OFFLOADED'),
         ('INPUT_ALIGN_OFFLOADED', 'PRC_ALIGNING'),
         ('PRC_ALIGNING', 'OFFLOADING_PRC'),
         ('OFFLOADING_PRC', 'PRC_ALIGN_OFFLOADED'),
         ('PRC_ALIGN_OFFLOADED', 'MICH_BRIGHT_ALIGNING'),
         ('MICH_BRIGHT_ALIGNING', 'OFFLOADING_MICH_BRIGHT'),
         ('OFFLOADING_MICH_BRIGHT', 'MICH_BRIGHT_OFFLOADED'),
         ('MICH_BRIGHT_OFFLOADED', 'SRC_ALIGNING'),
         ('SRC_ALIGNING', 'OFFLOADING_SRC'),
         ('OFFLOADING_SRC', 'SRC_ALIGN_OFFLOADED'),
         ('SRC_ALIGN_OFFLOADED', 'INIT_ALIGN_COMPLETE'),
         ('INIT_ALIGN_COMPLETE', 'UPDATE_SAFE_SNAP'),
         ('DOWN',                'UPDATE_SAFE_SNAP'),
         ('UPDATE_SAFE_SNAP',       'DOWN')
         ]
         
